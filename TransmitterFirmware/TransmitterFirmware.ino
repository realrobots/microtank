
#include <esp_now.h>
#include <WiFi.h>

#define LED 15
//84:F7:03:EB:B4:90

#define MID 98
#define DEADZONE 15


uint8_t buttonPins[] = {12, 3, 11, 7, 5, 9};
uint8_t buttonVals[] = {0,0,0,0,0,0};
uint8_t axisPins[] = {4, 6, 1, 2};
uint8_t axisVals[] = {127, 127, 127, 127};

// REPLACE WITH YOUR RECEIVER MAC Address
uint8_t broadcastAddress[] = {0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x04};

//uint8_t broadcastAddress[] = {0x84, 0xF7, 0x03, 0xEB, 0xB4, 0x90}; TEST

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message {
  uint8_t x;
  uint8_t y;
  uint8_t a;
  uint8_t b;
  uint8_t c;
} struct_message;

// Create a struct_message called myData
struct_message myData;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  //Serial.print("\r\nLast Packet Send Status:\t");
  if (status == ESP_NOW_SEND_SUCCESS){
    digitalWrite(LED, !digitalRead(LED));
  }
  //Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  for (int i = 0; i < 6; i++) {
    pinMode(buttonPins[i], INPUT_PULLUP);
  }
  for (int i = 0; i < 4; i++) {
    pinMode(axisPins[i], INPUT);
  }

  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
//
//  for (int i = 0; i < 8; i++){
//    pinMode(adcpins[i], INPUT_PULLUP);
//  }
}

uint8_t counter = 0;

 
void loop() {
  //for (int i = 0; i < 6; i++) {
    //Serial.print(digitalRead(buttonPins[i]));
    //Serial.print("\t");
  //}

  for (int i = 0; i < 4; i++) {
    axisVals[i] = map(analogRead(axisPins[i]), 0, 8191, 255, 0);
    if (axisVals[i] < MID - DEADZONE){
      axisVals[i] = map(axisVals[i], 0, MID - DEADZONE, 0, 127);
    } else if (axisVals[i] > MID + DEADZONE){
      axisVals[i] = map(axisVals[i], MID + DEADZONE, 255, 127, 255);
    } else {
      axisVals[i] = 127;
    }
    //Serial.print(axisVals[i]);
    //Serial.print("\t");
  }

  for (int i = 0; i < 6; i++){
    buttonVals[i] = digitalRead(buttonPins[i]);
  }

  Serial.print(axisVals[0]);
  Serial.print("\t");
  Serial.print(axisVals[3]);
  Serial.println();
  // Set values to send
  myData.x = axisVals[3];
  myData.y = axisVals[0];
  myData.a = buttonVals[0];  
  myData.b = buttonVals[1];
  myData.c = buttonVals[2];

  counter++;
  
  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
   
  if (result == ESP_OK) {
    //Serial.println("Sent with success");
    //digitalWrite(LED, !digitalRead(LED));
  }
  else {
    Serial.println("Error sending the data");
  }
  
//  for (int i = 0; i < 8; i++){
//    Serial.print(analogRead(adcpins[i]));
//    Serial.print("\t");
//  }
  Serial.println();
  
  delay(50);
}
