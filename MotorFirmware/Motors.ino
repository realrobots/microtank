#define EN 18
#define APWM 17
#define AIN2 16
#define AIN1 21

#define BPWM 35
#define BIN2 33
#define BIN1 34


const int freq = 5000;
const int ledChannel = 0;
const int resolution = 8;

void InitMotors() {
  pinMode(EN, OUTPUT);
  ledcSetup(ledChannel, freq, resolution);

  // attach the channel to the GPIO to be controlled
  ledcAttachPin(APWM, 0);
  ledcAttachPin(BPWM, 1);
  pinMode(AIN2, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);

  digitalWrite(EN, HIGH);

}

// 0 = full reverse, 127 = full stop, 255 = full forward
void Motor(uint8_t index, uint8_t power) {

  int pwm = map(power, 0, 255, -255, 255);

  if (index == 0) {
    
    if (power > 127) {
      digitalWrite(AIN1, HIGH);
      digitalWrite(AIN2, LOW);
      analogWrite(APWM, pwm);
    } else if (power < 127) {
      digitalWrite(AIN1, LOW);
      digitalWrite(AIN2, HIGH);
      analogWrite(APWM, -pwm);
    } else {
      digitalWrite(AIN1, LOW);
      digitalWrite(AIN2, LOW);
      analogWrite(APWM, pwm);
    }
  } else if (index == 1) {
    if (power > 127) {
      digitalWrite(BIN1, LOW);
      digitalWrite(BIN2, HIGH);
      analogWrite(BPWM, pwm);
    } else if (power < 127) {
      digitalWrite(BIN1, HIGH);
      digitalWrite(BIN2, LOW);
      analogWrite(BPWM, -pwm);
    } else {
      digitalWrite(BIN1, LOW);
      digitalWrite(BIN2, LOW);
      analogWrite(BPWM, pwm);
    }
  }
}


void MotorTest(){
  
  Motor(0, 255);
  Motor(1, 255);
  delay(500);
  
  Motor(0, 0);
  Motor(1, 0);
  delay(500);
  
  Motor(0, 127);
  Motor(1, 127);
}
