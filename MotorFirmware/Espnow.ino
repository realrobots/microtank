#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>

uint8_t newMACAddress[] = {0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x04};

int counter = 0;
long lastPacketReceived = 0;

uint8_t buttonState[] = {0,0,0};
uint8_t buttonPrev[] = {0,0,0};

typedef struct struct_message {
  uint8_t x;
  uint8_t y;
  uint8_t a;
  uint8_t b;
  uint8_t c;
} struct_message;


struct_message message;

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message, incomingData, sizeof(message));
  //  Serial.print("Bytes received: ");
  //  Serial.println(len);

  Motor(0, message.y);
  Motor(1, message.x);

  message.c = !message.c;

  if (message.c && !buttonPrev[2]){
    ToggleWeapon(0);
  } 

  buttonPrev[2] = message.c;


  digitalWrite(LED, !digitalRead(LED));
  Serial.print(message.x);
  Serial.print("\t");
  Serial.print(message.y);
  Serial.print("\t");
  Serial.print(message.a);
  Serial.print("\t");
  Serial.print(message.b);
  Serial.print("\t");
  Serial.print(message.c);
  Serial.println();

  lastPacketReceived = millis();
}


void InitESPNow() {
  WiFi.mode(WIFI_STA);
  Serial.println(WiFi.macAddress());

  esp_wifi_set_mac(WIFI_IF_STA, &newMACAddress[0]);


  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  esp_now_register_recv_cb(data_receive);
}

void PrintMacAddress(){
  
  Serial.println(WiFi.macAddress());
}

bool ESPTimeout(){
  long diff = millis() - lastPacketReceived;
  return diff > 2000 && diff != 0;
}

long GetLastPacketReceived(){
  return lastPacketReceived;
}

void SetLastPacketReceived(long t){
  lastPacketReceived = t;
}
