uint8_t weaponPins[] = {2, 3, 4};
uint8_t weaponState[] = {0,0,0};

void InitWeapons(){
  for (int i = 0; i < 3; i++){
    pinMode(weaponPins[i], OUTPUT);
  digitalWrite(weaponPins[i], 0);
  }
}

void SetWeapon(uint8_t idx, uint8_t val){
  weaponState[idx] = val;
  digitalWrite(weaponPins[idx], !val);
}

void ToggleWeapon(uint8_t idx){
  weaponState[idx] = !weaponState[idx];
  digitalWrite(weaponPins[idx], weaponState[idx]);
}
