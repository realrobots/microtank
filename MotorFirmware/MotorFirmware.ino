#define LED 15

long lastReport = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);
  Serial.begin(115200);
  FlashLED(2);
  InitESPNow();
  FlashLED(3);
  InitMotors();
  InitWeapons();
  FlashLED(4);

  pinMode(5, OUTPUT);

  MotorTest();
}

void loop() {
  if (millis() - lastReport > 2000) {
    PrintMacAddress();
    lastReport = millis();

    digitalWrite(5, !digitalRead(5));
  }

  if (ESPTimeout()) {
    Serial.print(millis());
    Serial.print("\t");
    Serial.println(GetLastPacketReceived());
    
    digitalWrite(LED, 1);
    Motor(0, 127);
    Motor(1, 127);
    SetWeapon(0, 1);
    SetLastPacketReceived(millis());
  }
}


void FlashLED(uint8_t flashCount) {
  digitalWrite(LED, 0);
  delay(50);
  for (int i = 0; i < flashCount; i++) {
    digitalWrite(LED, 1);
    delay(50);
    digitalWrite(LED, 0);
    delay(50);
  }
  digitalWrite(LED, 1);
}
