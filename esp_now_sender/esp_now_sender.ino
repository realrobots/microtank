
#include <esp_now.h>
#include <WiFi.h>


//84:F7:03:EB:B4:90

// REPLACE WITH YOUR RECEIVER MAC Address
uint8_t broadcastAddress[] = {0x84, 0xF7, 0x03, 0xEB, 0xB4, 0x90};

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message {
  uint8_t x;
  uint8_t y;
  uint8_t a;
  uint8_t b;
  uint8_t c;
} struct_message;

// Create a struct_message called myData
struct_message myData;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}

uint8_t counter = 0;
 
void loop() {
  // Set values to send
  myData.x = random(256);
  myData.y = random(256);
  myData.a = random(256);  
  myData.b = random(256);
  myData.c = counter;

  counter++;
  
  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
   
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }
  delay(50);
}
