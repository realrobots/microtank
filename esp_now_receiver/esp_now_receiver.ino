#include <esp_now.h>
#include <WiFi.h>

int counter = 0;

typedef struct struct_message {
    uint8_t x;
    uint8_t y;
    uint8_t a;
    uint8_t b;
    uint8_t c;
} struct_message;


struct_message message;

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message, incomingData, sizeof(message));
//  Serial.print("Bytes received: ");
//  Serial.println(len);
  Serial.print(message.x);
  Serial.print("\t");
  Serial.print(message.y);
  Serial.print("\t");
  Serial.print(message.a);
  Serial.print("\t");
  Serial.print(message.b);
  Serial.print("\t");
  Serial.print(message.c);
  Serial.println();
}
 
void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_recv_cb(data_receive);
}
 
void loop() {
//counter++;
//if (counter % 100 == 0){
//  Serial.println(WiFi.macAddress());
//}
}
