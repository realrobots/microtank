#define MID 98
#define DEADZONE 15


uint8_t buttonPins[] = {12, 3, 11, 7, 5, 9};
uint8_t axisPins[] = {4, 6, 1, 2};
uint8_t axisVals[] = {127, 127, 127, 127};



void setup() {
  // put your setup code here, to run once:
  for (int i = 0; i < 6; i++) {
    pinMode(buttonPins[i], INPUT_PULLUP);
  }
  for (int i = 0; i < 4; i++) {
    pinMode(axisPins[i], INPUT);
  }

  pinMode(15, OUTPUT);
  digitalWrite(15, 1);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  for (int i = 0; i < 6; i++) {
    Serial.print(digitalRead(buttonPins[i]));
    Serial.print("\t");
  }

  for (int i = 0; i < 4; i++) {
    axisVals[i] = map(analogRead(axisPins[i]), 0, 8191, 255, 0);
    if (axisVals[i] < MID - DEADZONE){
      axisVals[i] = map(axisVals[i], 0, MID - DEADZONE, 0, 127);
    } else if (axisVals[i] > MID + DEADZONE){
      axisVals[i] = map(axisVals[i], MID + DEADZONE, 255, 127, 255);
    } else {
      axisVals[i] = 127;
    }
    Serial.print(axisVals[i]);
    Serial.print("\t");
  }
  
  Serial.println();
  delay(50);
}
